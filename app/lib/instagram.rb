require "sinatra"
require 'instagram'
require 'httparty'
require 'json'

class Instagram
	 
	def self.authorize_url(callback)
		CALLBACK_URL = "http://localhost:4567/oauth/callback"
		CLIENT_ID = ENV["INSTAGRAM_CLIENT_ID"]
		CLIENT_SECRET = ENV["INSTAGRAM_CLIENT_SECRET"]
	end
end