class CreateIdentities < ActiveRecord::Migration
  def change
    create_table :identities do |t|
      t.references :user, index: true, foreign_key: true
      t.text :provider
      t.text :uid
      t.text :auth
      t.timestamps null: false
    end
  end
end
