Login with Instagram Rails

*Ruby version*
2.2.3

*Database creation*
Postgresql 9.x

*Database initialization*
Mac: 

    brew install postgresql 

Linux:

    sudo apt-get install postgresql

*Setup*

    rvm install ruby -v 2.2.3
    gem install bundler
    bundle install
    brew install ImageMagick # (on linux, sudo apt-get install ImageMagick)
    psql -c "create role photoer with superuser login"
    psql -c "create database photocation_dev;"
    psql -c "create database photocation_test"
    rake db:migrate
    rails s
    in browser, open http://localhost:3000